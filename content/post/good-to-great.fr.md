+++
date = "2017-05-07T13:40:42-04:00"
title = "good to great"
draft = true

+++

I read **Good to Great in January 2016**. An awesome read sharing detailed analysis on how good companies became great.
<!--more-->
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin dictum, arcu quis congue accumsan, quam mi ullamcorper dui, ut feugiat elit nulla ac sapien. Vestibulum felis dui, pulvinar ut sodales vitae, mattis tincidunt est. Aenean auctor, nisl quis ornare rhoncus, purus diam euismod nisi, at elementum erat felis non leo. Fusce purus erat, condimentum eget aliquet eu, tempor aliquam massa. Sed nunc ligula, sagittis et ligula rutrum, viverra sodales tellus. Fusce eget placerat odio. Sed ac mattis ipsum. Praesent fermentum eu lorem nec consectetur. Nulla commodo ipsum nisl, sit amet dapibus justo tristique in. Mauris condimentum in massa vitae malesuada. Donec id nibh eget urna mattis luctus id et elit. Integer ac bibendum enim, id porttitor massa. In sit amet sagittis mauris. Donec eget ipsum sapien. In lacinia dapibus tempus. Curabitur non vulputate sapien.

Integer placerat pretium orci, a pellentesque diam consequat ut. Praesent rhoncus leo at augue gravida sollicitudin. Mauris posuere nibh eget aliquam laoreet. Vestibulum interdum est urna, ut laoreet justo rutrum eu. Integer nec varius ante. Aliquam in mauris tempus lorem venenatis pellentesque. Nunc elementum ultrices arcu, at mollis risus semper ac.

Fusce ac lacus mattis, convallis neque ut, eleifend nisl. Interdum et malesuada fames ac ante ipsum primis in faucibus. Fusce pellentesque varius vulputate. Nullam consectetur ex vitae euismod consectetur. Cras maximus tristique quam sit amet finibus. Nam convallis tellus augue, venenatis euismod nunc iaculis et. In vel metus at massa imperdiet bibendum. Sed ultricies a libero pellentesque maximus. Maecenas vitae tellus pretium urna consequat sodales. Ut quam sem, placerat vitae nibh sed, feugiat molestie velit. Cras tristique malesuada nisi, non condimentum metus placerat id. Donec tempus, magna a bibendum laoreet, massa ligula lobortis lacus, quis sollicitudin velit nisl id ante. Morbi pulvinar lacus ac erat maximus, eget vulputate ligula facilisis. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.

Sed ullamcorper efficitur condimentum. Interdum et malesuada fames ac ante ipsum primis in faucibus. Donec id egestas nunc. Duis efficitur vulputate massa, in pulvinar nulla efficitur eget. Fusce placerat eros lacinia, auctor ipsum quis, interdum libero. In laoreet et felis vel mattis. Quisque ut tortor sit amet eros elementum commodo ac non magna. Phasellus sed nisi ut mi lacinia vulputate ut eget orci. Nunc posuere tempus hendrerit. Nunc egestas dolor nec tristique vehicula. Aliquam elit justo, volutpat in metus id, volutpat fringilla felis. Morbi ultricies nulla eget lectus aliquet, a aliquet ex laoreet. Donec et libero risus. Fusce euismod aliquet congue. Suspendisse arcu lacus, eleifend a purus pellentesque, auctor venenatis sem. Curabitur lacinia purus ut quam tempor sollicitudin. 
